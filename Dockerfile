FROM debian:buster

LABEL org.label-schema.name="Debian Buster" \
      org.label-schema.description="baseimage based on phusion/baseimage debian edition" \
      org.label-schema.vcs-url="https://gitlab.com/tigefa/debian-baseimage-docker"

COPY . /bd_build

RUN /bd_build/prepare.sh && \
  	/bd_build/system_services.sh && \
  	/bd_build/utilities.sh && \
  	/bd_build/cleanup.sh

ENV DEBIAN_FRONTEND="teletype" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

CMD ["/sbin/my_init"]
